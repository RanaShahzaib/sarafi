package af.sarafi.amiri.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import af.sarafi.amiri.CurrencyConversionActivity;
import af.sarafi.amiri.Model.Item;
import af.sarafi.amiri.R;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Shahzaib on 8/23/2017.
 */

public class Adapter extends BaseAdapter {

    ArrayList<Item> mList = new ArrayList<>();

    public Adapter(ArrayList<Item> mList) {
        this.mList = mList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView mTextView1;
        TextView mTextView2;
        TextView mTextView3;
        TextView mTextView0;
        ImageView mImageView;
        LinearLayout mLinearLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            mTextView1  = (TextView)itemView.findViewById(R.id.textView1);
            mTextView2  = (TextView)itemView.findViewById(R.id.textView2);
            mTextView3  = (TextView)itemView.findViewById(R.id.textView3);
            mTextView0 = (TextView)itemView.findViewById(R.id.textView0);
            mImageView = (ImageView)itemView.findViewById(R.id.flag);
            mLinearLayout = (LinearLayout)itemView.findViewById(R.id.container);
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.list_item, parent, false);

        MyViewHolder holder = new MyViewHolder(view);
        final Item item = mList.get(position);
        String mStringDate = item.getData1();
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date mFormatedDate = mFormat.parse(mStringDate);
            SimpleDateFormat mDD = new SimpleDateFormat("hh:mm yyyy-MM-dd");
            String newDate = mDD.format(mFormatedDate);
            holder.mTextView1.setText(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mTextView2.setText(item.getData2());
        holder.mTextView3.setText(item.getData3());
        holder.mTextView0.setText(item.getData0());
        try{
            Picasso.with(holder.mImageView.getContext()).load(item.getFlag()).into(holder.mImageView);
        }catch (Exception ex){
            Log.v("ImageError", "Image ID : "+item.getFlag());
        }

        Log.v("PPPPOSTIO",""+position);
        if ((position+1)%5==0){
            AdView adView = new AdView(holder.mImageView.getContext());
            adView.setAdUnitId("ca-app-pub-4430410888027043/2530554253");
            adView.setAdSize(AdSize.LARGE_BANNER);
            holder.mLinearLayout.addView(adView);
            AdRequest request = new AdRequest.Builder().build();
            adView.loadAd(request);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), CurrencyConversionActivity.class);
                intent.putExtra("name", item.getData0());
                view.getContext().startActivity(intent);
            }
        });
        return view;
    }

}
