package af.sarafi.amiri.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import af.sarafi.amiri.Model.ItemNav;
import af.sarafi.amiri.R;

import java.util.ArrayList;

/**
 * Created by Shahzaib on 8/14/2017.
 */

public class NavigationDrawerAdapter extends ArrayAdapter {
    ArrayList<ItemNav> mList = ItemNav.getNavMenuItems();
    //NavItemSelectedListener mCallback;

    AdapterView.OnItemClickListener onItemClickListener;

    final int COLORS[] = {Color.parseColor("#ffbf00"),Color.parseColor("#94E7EF"),Color.parseColor("#ED8A19"),
            Color.parseColor("#EBBA16"),Color.parseColor("#94E7EF")};
    //DrawerLayout mDrawer;

    public NavigationDrawerAdapter(@NonNull Context context, @LayoutRes int resource, AdapterView.OnItemClickListener onItemClickListener) {
        super(context, resource);
    //    this.mDrawer = mDrawer;
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnNavItemSelectedListener(NavItemSelectedListener listener){
    //    mCallback = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ItemNav item = mList.get(position);

        if(item.isHeader()){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_main, parent, false);
            return convertView;
        }else{
//            if(convertView==null){
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_item, parent, false);
//            }
            final ImageView icon = (ImageView)convertView.findViewById(R.id.icon);
            final TextView title = (TextView)convertView.findViewById(R.id.text);
            icon.setImageResource(item.getImg());
            title.setText(item.getTitle());
            final View vv = convertView.findViewById(R.id.highlight);

            if(item.isChecked()){
                vv.setBackgroundColor(COLORS[position]);
            }else{
                vv.setBackgroundColor(Color.parseColor("#ffffff"));
            }

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
//                    mDrawer.closeDrawer(GravityCompat.START);
//                }
                for(int j=0; j<mList.size(); j++){
                    ItemNav i = mList.get(j);
                    i.setChecked(false);
                }
                item.setChecked(true);
                notifyDataSetChanged();

//                mCallback.onNavItemSelected(position);
                onItemClickListener.onItemClick(null, null, position, 0);
            }
        });
            return convertView;
        }


    }

    @Override
    public int getCount() {
        return mList.size();
    }

    public interface NavItemSelectedListener{
        void onNavItemSelected(int position);
    }
}
