package af.sarafi.amiri.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import af.sarafi.amiri.Adapters.Adapter;
import af.sarafi.amiri.Currency;
import af.sarafi.amiri.Helpers.Helper;
import af.sarafi.amiri.Model.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import af.sarafi.amiri.R;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Shahzaib on 8/23/2017.
 */

public class HomeFragment extends Fragment {
    ListView mRecyclerView;
    ArrayList<Item> items = new ArrayList<>();
    Adapter mAdapter;
    ProgressBar mProgress;
    FragmentManager fm;
    private static final String TAG = HomeFragment.class.getSimpleName();

    public void setFm(FragmentManager fm){
        this.fm = fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.layout_fragment_main, container, false);
        mRecyclerView = (ListView) view.findViewById(R.id.recyclerView);
        mProgress = (ProgressBar)view.findViewById(R.id.progress);
        callNetwork();
        return view;
    }


    void callNetwork(){
//        final StringRequest request = new StringRequest(Request.Method.GET, "http://ajelkhabar.com/sarafi/currency.php", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try{
//                    Helper.rates.clear();
//                    mProgress.setVisibility(View.GONE);
//                    JSONArray array = new JSONArray(response);
//                    items.clear();
//                    for(int i=0; i<array.length(); i++){
//                        JSONObject object = array.getJSONObject(i);
//                        String name = object.getString("name");
//                        String buyRate = object.getString("buy_rate");
//                        String sellRate = object.getString("sell_rate");
//                        String timeStamp = object.getString("last_updated");
//                        Helper.rates.put(name, Double.valueOf(sellRate));
//                        items.add(new Item(name, timeStamp, buyRate, sellRate, getImage(name)));
//                    }
//                    Log.v("length",""+items.size());
//                    mAdapter = new Adapter(items);
//                    mRecyclerView.setAdapter(mAdapter);
//                    mAdapter.notifyDataSetChanged();
//                }catch (Exception ex){
//                    mProgress.setVisibility(View.GONE);
//                    Toast.makeText(getContext(), "Error getting response", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(getContext(), "Error getting response", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        queue.add(request);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://www.sarafi.af/api/api.php?key=OTUwMDg4NDA5Mjc2ODQ3MzgyNw==&username=sarafiaf&pass=YWRtaW4", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject object = new JSONObject(result);
                    JSONArray currencies = object.getJSONArray("currencies");
                    for(int i=0; i<currencies.length(); i++){
                        JSONObject currency = currencies.getJSONObject(i);
                        String buyRate = currency.getString("buy_rate");
                        String sellRate = currency.getString("sell_rate");
                        String date = currency.getString("updated_on");
                        String name = currency.getString("cc");
                        Helper.rates.put(name, Double.valueOf(sellRate));
                        items.add(new Item(name, date, buyRate, sellRate, getImage(name)));
                    }
                    mAdapter = new Adapter(items);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_view_footer, null, false);
                    mRecyclerView.addFooterView(footerView);
                }catch (JSONException ex){
                    Log.d(TAG, "Response : "+result+"\n"+"Error : "+ex.getLocalizedMessage());
                    Toast.makeText(getActivity(),"Bad response from server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getActivity(), "Error"+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    int getImage(String str){
        int img = 0;
        switch (str){
            case Currency.AED:{
                img = R.drawable.emirates;
                break;
            }case Currency.AUD:{
                img = R.drawable.australia;
                break;
            }case Currency.BHD:{
                img = R.drawable.bahrain;
                break;
            }case Currency.CAD:{
                img = R.drawable.canada;
                break;
            }case Currency.CHF:{
                img = R.drawable.swiss;
                break;
            }case Currency.CNY:{
                img = R.drawable.china;
                break;
            }case Currency.DKK:{
                img = R.drawable.denmark;
                break;
            }case Currency.EUR:{
                img = R.drawable.europe;
                break;
            }case Currency.GBP:{
                img = R.drawable.england;
                break;
            }case Currency.INR:{
                img = R.drawable.india;
                break;
            }case Currency.IRR:{
                img = R.drawable.iran;
                break;
            }case Currency.JOD:{
                img = R.drawable.jordin;
                break;
            }case Currency.KWD:{
                img = R.drawable.kwd;
                break;
            }case Currency.NOK:{
                img = R.drawable.norway;
                break;
            }case Currency.OMR:{
                img = R.drawable.oman;
                break;
            }case Currency.PKR:{
                img = R.drawable.pakistan;
                break;
            }case Currency.QAR:{
                img = R.drawable.qatar;
                break;
            }case Currency.RUB:{
                img = R.drawable.russia;
                break;
            }case Currency.SAR:{
                img = R.drawable.saudi_arabia;
                break;
            }case Currency.SEK:{
                img = R.drawable.sweden;
                break;
            }case Currency.TJS:{
                img = R.drawable.tajikistan;
                break;
            }case Currency.TMT:{
                img = R.drawable.turkmanistan;
                break;
            }case Currency.TRY:{
                img = R.drawable.turkey;
                break;
            }case Currency.USD:{
                img = R.drawable.usa;
                break;
            }
        }
        return img;
    }

}
