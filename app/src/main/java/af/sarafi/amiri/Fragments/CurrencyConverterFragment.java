package af.sarafi.amiri.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import af.sarafi.amiri.Helpers.Helper;
import af.sarafi.amiri.R;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DecimalFormat;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyConverterFragment extends Fragment {

    TextView mC1;
    TextView mC2;
    EditText mR1;
    EditText mR2;

    public CurrencyConverterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_currency_converter, container, false);
        mC1 = (TextView)view.findViewById(R.id.c1);
        mC2 = (TextView)view.findViewById(R.id.c2);

        AdView mAdView = (AdView)view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mR1 = (EditText)view.findViewById(R.id.r1);
        mR2 = (EditText)view.findViewById(R.id.r2);

        mR1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus&&mR1.getText().toString().equals("0")){
                    mR1.setText("");
                }
            }
        });

        mR1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                double rate;
                int tv;

                try{
                    rate = Helper.rates.get(mC1.getText().toString());
                    tv = Integer.parseInt(s.toString());
                }catch (Exception ex){
                    rate = 0;
                    tv = 0;
                }
                double total = rate*tv;

//                if(String.valueOf(total).length()>10){
//                    ViewGroup.LayoutParams params = mR2.getLayoutParams();
//                    //int width = params.width;
//                    int width = 150+String.valueOf(total).length()-10;
//                    params.width = width;
//                    mR2.setLayoutParams(params);
//
//                    ViewGroup.LayoutParams params1 = mC2.getLayoutParams();
//                    //int width2 = params1.width;
//                    int width2 = 150-String.valueOf(total).length()-10;
//                    params1.width = width2;
//                    mC2.setLayoutParams(params1);
//                }

                mR2.setText((total)%1==0?String.valueOf(new Double(total).intValue()):String.valueOf((new DecimalFormat("#.00").format(rate*tv))));
                Log.v("CurrencyConverter","Currency : "+mC2.getText().toString()+" Rate : "+total);

            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mC1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenu(1);
            }
        });
//        mC2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openMenu(2);
//            }
//        });
        return view;
    }


    void openMenu(final int flag){
        PopupMenu popup = new PopupMenu(getContext() , flag == 1 ? mC1:mC2);
        popup.getMenuInflater()
                .inflate(R.menu.currencies, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                mR1.setText("");
                mR2.setText("0");
                switch (flag){
                    case 1:{
                        mC1.setText(item.getTitle());
                        break;
                    }case 2:{
                        mC2.setText(item.getTitle());
                        break;
                    }
                }
                return true;
            }
        });

        popup.show();
    }
}
