package af.sarafi.amiri.Fragments;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.messaging.FirebaseMessaging;

import af.sarafi.amiri.Helpers.PreferencesHelper;
import af.sarafi.amiri.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener{


    public SettingsFragment() {
        // Required empty public constructor
    }
    SwitchCompat mSwitch;
    PreferencesHelper mHelper;
    TextView fb, insta, twitter;
    ImageView checkUpdates, tellFriends;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);
        mSwitch = (SwitchCompat)view.findViewById(R.id.notification);
        mHelper = new PreferencesHelper(getContext());
        mSwitch.setChecked(mHelper.isNotificationOn());
        checkUpdates = view.findViewById(R.id.check_updates);
        tellFriends = view.findViewById(R.id.tell_friends);
        checkUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkUpdates();
            }
        });
        tellFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareApp();
            }
        });
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mHelper.setNotification(isChecked);
                if(isChecked){
                    FirebaseMessaging.getInstance().subscribeToTopic("public");
                }else{
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("public");
                }
            }
        });
        AdView mAdView = (AdView)view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        fb = (TextView)view.findViewById(R.id.fb);
        insta = (TextView)view.findViewById(R.id.insta);
        twitter = (TextView)view.findViewById(R.id.twitter);
        fb.setOnClickListener(this);
        insta.setOnClickListener(this);
        twitter.setOnClickListener(this);
        return view;
    }

    void checkUpdates(){
        try{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
        }catch (ActivityNotFoundException ex){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
        }
    }

    void shareApp(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Check out the Sarafi  app at: "+"https://play.google.com/store/apps/details?id="+getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+((TextView)view).getText().toString()));
        startActivity(browserIntent);
    }

    String getPackageName(){
        return getActivity().getApplicationContext().getPackageName();
    }
}
