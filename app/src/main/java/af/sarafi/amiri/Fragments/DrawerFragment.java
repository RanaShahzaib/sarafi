package af.sarafi.amiri.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import af.sarafi.amiri.Adapters.NavigationDrawerAdapter;
import af.sarafi.amiri.R;


/**
 * Created by Shahzaib on 8/13/2017.
 */

public class DrawerFragment extends Fragment {

    ActionBarDrawerToggle mDrawerToggle;
    View view;
    NavigationDrawerAdapter.NavItemSelectedListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drawer, container, false);
        this.view = view;
        return view;
    }

//    public void setUpList(View view, DrawerLayout mDrawer){
//        ListView mListView = (ListView)view.findViewById(R.id.left_drawer);
//        NavigationDrawerAdapter mAdapter = new NavigationDrawerAdapter(getContext(), R.layout.navigation_item);
//        mAdapter.setOnNavItemSelectedListener(listener);
//        mListView.setAdapter(mAdapter);
//    }

//    public void setUpDrawer(int nav_drawer_fragment, DrawerLayout drawerLayout, Toolbar toolbar) {
//        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
//        drawerLayout.addDrawerListener(mDrawerToggle);
//        drawerLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                mDrawerToggle.syncState();
//            }
//        });
//        setUpList(view, drawerLayout);
//    }

    public void setOnNavItemSelectedListener(NavigationDrawerAdapter.NavItemSelectedListener listener){
        this.listener = listener;
    }

}
