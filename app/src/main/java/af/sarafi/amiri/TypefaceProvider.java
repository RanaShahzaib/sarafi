package af.sarafi.amiri;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Shahzaib on 8/23/2017.
 */

public class TypefaceProvider {
    public static Typeface getTypeface(Context context){
        return Typeface.createFromAsset(context.getAssets(), "Museo300-Regular.otf");
    }
}
