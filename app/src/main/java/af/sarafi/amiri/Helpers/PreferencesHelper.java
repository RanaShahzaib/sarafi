package af.sarafi.amiri.Helpers;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Shahzaib on 9/20/2017.
 */

public class PreferencesHelper {
    Context mContext;

    private static final String KEY_NOTIFICATION = "notification";

    public PreferencesHelper(Context mContext) {
        this.mContext = mContext;
    }

    public void setNotification(boolean key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(KEY_NOTIFICATION, key).apply();
    }

    public boolean isNotificationOn(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(KEY_NOTIFICATION, false);
    }
}
