package af.sarafi.amiri.Model;

/**
 * Created by Shahzaib on 8/23/2017.
 */

public class Item {
    private String data0;
    private String data1;
    private String data2;
    private String data3;
    private int flag;

    public Item(String data0, String data1, String data2, String data3, int flag) {
        this.data0 = data0;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;
        this.flag = flag;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getData0() {
        return data0;
    }

    public void setData0(String data0) {
        this.data0 = data0;
    }


    @Override
    public String toString() {
        return data0+", "+data1+", "+data2+", "+data3;
    }
}
