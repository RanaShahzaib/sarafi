package af.sarafi.amiri.Model;


import java.util.ArrayList;

import af.sarafi.amiri.R;

/**
 * Created by Shahzaib on 8/14/2017.
 */

public class ItemNav {
    String title;
    int img;
    boolean checked;
    boolean isHeader = false;



    public ItemNav(String title, int img) {
        this.title = title;
        this.img = img;
        checked = false;
    }

    public ItemNav(){
        isHeader = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTitle() {
        return title;
    }

    public int getImg() {
        return img;
    }

    public boolean isHeader(){
        return isHeader;
    }

    public static ArrayList<ItemNav> getNavMenuItems(){
        final int ICONS[] = {R.drawable.ic_home, R.drawable.ic_calculator, R.drawable.ic_settings, R.drawable.ic_info};
        final String TITLES[] = {"Home", "Currency Convertor", "Settings", "About"};

        ArrayList<ItemNav> items = new ArrayList<>();
        ItemNav header = new ItemNav();
        items.add(header);
        ItemNav item = new ItemNav(TITLES[0], ICONS[0]);
        item.setChecked(true);
        items.add(item);
        items.add(new ItemNav(TITLES[1], ICONS[1]));
        items.add(new ItemNav(TITLES[2], ICONS[2]));
        items.add(new ItemNav(TITLES[3], ICONS[3]));

        return items;
    }
}
