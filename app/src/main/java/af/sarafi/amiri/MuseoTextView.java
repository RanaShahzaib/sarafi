package af.sarafi.amiri;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Shahzaib on 8/23/2017.
 */

public class MuseoTextView extends TextView {
    public MuseoTextView(Context context) {
        super(context);
        setTypeface(getTypeFace(context));
    }

    public MuseoTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTypeface(getTypeFace(context));
    }

    public MuseoTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(getTypeFace(context));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MuseoTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setTypeface(getTypeFace(context));
    }

    Typeface getTypeFace(Context context){
        return Typeface.createFromAsset(context.getAssets(), "Museo300-Regular.otf");
    }
}
