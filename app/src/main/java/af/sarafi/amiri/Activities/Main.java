package af.sarafi.amiri.Activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import af.sarafi.amiri.Adapters.NavigationDrawerAdapter;
import af.sarafi.amiri.Fragments.AboutFragment;
import af.sarafi.amiri.Fragments.CurrencyConverterFragment;
import af.sarafi.amiri.Fragments.HomeFragment;
import af.sarafi.amiri.Fragments.SettingsFragment;
import af.sarafi.amiri.R;

public class Main extends AppCompatActivity implements NavigationDrawerAdapter.NavItemSelectedListener {

    ListView mRecyclerView;
    NavigationView navigationView;

    Toolbar toolbar;
    DrawerLayout mDrawerLayout;
    AdapterView.OnItemClickListener mItemsClickListener;
    TextView mToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mToolbarTitle = (TextView)findViewById(R.id.toolbar_text);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(drawer.isDrawerVisible(GravityCompat.START)){
                    drawer.closeDrawer(GravityCompat.START);
                }else{
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        mRecyclerView = (ListView) findViewById(R.id.recyclerView);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
        mItemsClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onNavItemSelected(position);
            }
        };
        setUpDrawer();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icon) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void setUpDrawer(){
//        DrawerFragment fragment = (DrawerFragment)getSupportFragmentManager().findFragmentById(R.id.nav_drawer_fragment);
//        fragment.setOnNavItemSelectedListener(this);
//        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
//        fragment.setUpDrawer(R.id.nav_drawer_fragment, drawerLayout, toolbar);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        View footerView = ((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_view_footer, null, false);
        mDrawerList.addFooterView(footerView);
        NavigationDrawerAdapter mAdapter = new NavigationDrawerAdapter(this, R.layout.navigation_item, mItemsClickListener);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(mItemsClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onNavItemSelected(int position) {
        if(position == 1){
            //startActivity(new Intent(this, Main.class));
            mToolbarTitle.setText("SARAFI.AF");
            FragmentManager fragmentManager = getSupportFragmentManager();
            HomeFragment fragment = new HomeFragment();
            fragment.setFm(fragmentManager);
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        }else if(position == 2 ){
            mToolbarTitle.setText("CURRENCY CONVERTER");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, new CurrencyConverterFragment()).commit();
        }else if(position == 3){
            mToolbarTitle.setText("SETTINGS");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, new SettingsFragment()).commit();
        }else if(position == 4){
            mToolbarTitle.setText("ABOUT");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, new AboutFragment()).commit();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }
}
