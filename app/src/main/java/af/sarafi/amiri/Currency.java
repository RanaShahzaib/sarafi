package af.sarafi.amiri;

/**
 * Created by Shahzaib on 8/30/2017.
 */

public class Currency {

    public static final String USD = "USD";
    public static final String EUR = "EUR";
    public static final String GBP = "GBP";
    public static final String SAR = "SAR";
    public static final String AED = "AED";
    public static final String CHF = "CHF";
    public static final String AUD = "AUD";
    public static final String CAD = "CAD";
    public static final String INR = "INR";
    public static final String IRR = "IRR";
    public static final String PKR = "PKR";
    public static final String QAR = "QAR";
    public static final String DKK = "DKK";
    public static final String SEK = "SEK";
    public static final String NOK = "NOK";
    public static final String TRY = "TRY";
    public static final String TJS = "TJS";
    public static final String TMT = "TMT";
    public static final String CNY = "CNY";
    public static final String JOD = "JOD";
    public static final String KWD = "KWD";
    public static final String BHD = "BHD";
    public static final String OMR = "OMR";
    public static final String RUB = "RUB";

}
